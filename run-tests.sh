#!/bin/bash

set -e

variant="$1"
app="org.apertis.builder.$variant.hello"

# HACK: manually install the runtime as a workaround for missing:
# https://github.com/flatpak/flatpak/pull/4316
flatpak --user -y install deps org.apertis."$variant".Platform//v2022 \
  --arch="$FLATPAK_ARCH"

flatpak --user remote-add --no-gpg-verify test "$PWD/$TARGETS_REPO"
flatpak --user -y install test "$app" --arch="$FLATPAK_ARCH"

hello=$(flatpak run "$app" user)
echo "##### $variant: $hello"
